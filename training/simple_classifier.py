"""
Simple classifier for doing runs with sklearn implementations of common non-neural net ML algorithms
"""

import json
import re
from collections import Counter
from datetime import datetime

from joblib import dump
import numpy as np
import sklearn.metrics as metrics
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import cross_validate
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

from utils.tokenizer import LemmaTokenizer, PorterTokenizer
from utils.document_cleaning_util import extract_article

class SimpleClassifier():
    """
    Sklearn based classifier class
    """

    def __init__(self, config):
        self.config = config
        print("Initialising new SimpleClassifier")

        self.x = None
        self.raw_x = []
        self.y = []
        self.tfidf_transformer = TfidfTransformer()
        self.vectorizer = None # note this will get initialized during preprocessing

        self._import_data()
        self._preprocess_data()

    def _import_data(self):
        """
        Imports training from the data directory
        """

        data_path = "./data/Jan2020Frontiers.jsonl"
        with open(data_path, 'r') as json_file:
            json_list = list(json_file)
            print(len(json_list), "total articles loaded")

            # build skip list
            skip_list = []
            counts = {}
            remove_under = self.config.get("remove_under", 10)
            if remove_under:
                for article in json_list:
                    obj = json.loads(article)
                    journal = obj['journal']
                    if journal in counts.keys():
                        counts[journal] += 1
                    else:
                        counts[journal] = 1

                for key, item in counts.items():
                    if item < remove_under:
                        print("Low Data, Skipping", key)
                        skip_list.append(key)

            for article in json_list:
                obj = json.loads(article)
                journal = obj['journal']

                if journal not in skip_list:
                    # append data to x and y
                    self.raw_x.append(
                        extract_article(obj['text'])
                    )
                    self.y.append(obj['journal'])

            print("Filtered Journal counts:", Counter(self.y))
            print("Loading Done")

    def _preprocess_data(self):
        """
        Runs preprocessing steps on the useable text

        Vectorizing & tfidf
        """
        print("Running Pre-Processing")

        tokenizer = None
        tokenizer_str = self.config.get('tokenizer', 'lemma')
        if tokenizer == 'lemma':
            tokenizer = LemmaTokenizer()
        elif tokenizer_str == 'porter':
            tokenizer = PorterTokenizer()

        self.vectorizer = CountVectorizer(
            tokenizer=tokenizer,
            ngram_range=self.config.get('ngram_range', (1, 1))
        )
        x_counts = self.vectorizer.fit_transform(self.raw_x)
        self.x = self.tfidf_transformer.fit_transform(x_counts)

        print("X shape", self.x.shape)
        print("Done Pre-Processing")

    def classify(self):
        """
        Does classification run
        """

        x_train, x_test, y_train, y_test = train_test_split(
            self.x, self.y, test_size=0.20)

        clf = self._clf_selector()
        clf.fit(x_train, y_train)
        preds = clf.predict(x_test)
        y_test = np.array(y_test)
        incorrect = y_test[y_test != preds]
        print(Counter(incorrect))
        print("Accuracy: ", metrics.accuracy_score(y_test, preds))
        print(
            "Balanced Accuracy: ",
            metrics.balanced_accuracy_score(y_test, preds)
        )

    def classify_cross_val(self):
        """
        Uses cross validation for metric calculation
        """
        print("Starting Cross Val Run")
        cross_vals = 5
        clf = self._clf_selector()
        cv_results = cross_validate(
            clf,
            self.x,
            self.y,
            cv=cross_vals,
            n_jobs=5,
            verbose=3,
            scoring=('balanced_accuracy', 'accuracy')
        )
        print("Test accuracy:", cv_results['test_accuracy'])
        print(
            "Test balanced accuracy:",
            cv_results['test_balanced_accuracy']
        )
        print("Mean metric",
              sum(cv_results['test_accuracy'])/ cross_vals
        )
        print("Fit time:", cv_results['fit_time'])
        return cv_results

    def train_and_dump(
            self,
            filename=datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    ):
        """
        Trains and dumps a classifier
        """
        clf = self._clf_selector()
        clf.fit(self.x, self.y)
        dump(clf, 'models/' + filename + '.joblib')

        ## also dump the relevant transfomers
        dump(self.tfidf_transformer,
             'models/' + filename + '-tfidf-transformer.joblib')
        dump(self.vectorizer,
             'models/' + filename + '-vectorizer.joblib')

    def _clf_selector(self):
        """
        Looks at the config and builds the classifier object
        """
        clf_string = self.config.get("clf", 'SVC')
        if clf_string == 'SVC':
            return SVC(
                class_weight='balanced'
            )
        if clf_string == 'naive_bayes':
            return MultinomialNB()
        if clf_string == 'SGD':
            return SGDClassifier()

        raise NotImplementedError("Invalid Classifier String")

if __name__ == "__main__":
    CLASSIFIER = SimpleClassifier(
        {
            'ngram_range': (1, 1),
            'tokenizer': 'porter',
            'clf_string': 'SGD'
        }
    )
    print("Running training")
    CLASSIFIER.train_and_dump()
