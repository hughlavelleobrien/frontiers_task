"""
Does a grid search using sacred
"""

from sacred_run_sklearn import ex
from sklearn.model_selection import ParameterGrid

def full_loop():
    """
    Full loop on all params
    """
    grid = {
        'classifiers': ['SVC', 'naive_bayes', 'SGD'],
        'tokenizers' : ['porter', 'lemma'],
        'ngram_ranges': [(1, 1), (2, 2), (1, 2)]
    }
    for entry in ParameterGrid(grid):
        ex.run(config_updates={
            'clf_string': entry['classifiers'],
            'tokenizer': entry['tokenizers'],
            'ngram_range': entry['ngram_ranges']
        })

full_loop()
