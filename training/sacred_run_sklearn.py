"""
Optimisation runs, stores results in Sacred MongoDB

See https://github.com/IDSIA/sacred for docs
"""

import numpy as np
from sacred.observers import MongoObserver
from sacred import Experiment, SETTINGS
from simple_classifier import SimpleClassifier

# https://github.com/IDSIA/sacred/issues/705
SETTINGS['CAPTURE_MODE'] = 'sys'

def connect_db(experiment):
    """
    Connect to the remote mongodb
    """
    user_name, password = fetch_env()

    mongo_url = "mongodb+srv://" + user_name + ":" + password + "@sacred.67v47.mongodb.net"
    observer = MongoObserver(
        url=mongo_url,
        db_name="sacred"
    )
    experiment.observers.append(observer)
    print(mongo_url)
    return experiment

def fetch_env():
    """
    Returns mongo username and password from .env file
    """
    u_name = ""
    password = ""
    with open(".env") as content:
        for line in content.readlines():
            splits = line.split("=")
            if splits[0] == "MONGO_INITDB_ROOT_USERNAME":
                u_name = splits[1][:-1] # -1 to remove the line break
            elif splits[0] == "MONGO_INITDB_ROOT_PASSWORD":
                password = splits[1][:-1] # -1 to remove the line break
    return (u_name, password)


ex = Experiment("Frontiers")
ex = connect_db(ex)

@ex.config
def cfg():
    """
    Sacred configuration builder
    """
    ngram_range = (1, 1)
    tokenizer = "porter"
    clf_string = "SGD"

    config = {
        "ngram_range": ngram_range,
        "tokenizer": tokenizer,
        "clf_string": clf_string
    }

@ex.automain
def run(_run, _log, _seed, config):
    """
    Does run of cross validated with given configs
    """
    print(config)

    clf = SimpleClassifier(config)
    results = clf.classify_cross_val()
    _run.log_scalar("Accuracy", np.mean(results['test_score']))
