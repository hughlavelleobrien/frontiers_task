"""
Tests for document cleaniug utility
"""
import json

from utils.document_cleaning_util import extract_article

def test_extract_article():
    """
    Article extraction tests
    """
    with open('utils/util_test_data.json') as f:
        test_data = json.load(f)

    test_article = test_data[0]
    cleaned_article = extract_article(test_article)

    # The end should be the last sentence of the conclusion
    assert cleaned_article[-111:] == \
        "this can be extended to prognosticate the future risk of other type of ailments particularly chronic diseases  "

    # the start should be the title in lower case
    assert cleaned_article[:52] == "a hybrid approach for  diabetes mellitus progression"

    # the author names should be gone
    assert "karim keshavjee" not in cleaned_article
    assert "muhammad shahbaz" not in cleaned_article
