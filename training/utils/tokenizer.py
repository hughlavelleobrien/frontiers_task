"""
Tokenizer classes for using nltk stemmers
"""

from nltk import download as nltk_download
from nltk import word_tokenize
from nltk.stem import PorterStemmer, WordNetLemmatizer

nltk_download('punkt')
nltk_download('wordnet')

class LemmaTokenizer:
    def __init__(self):
        self.wnl = WordNetLemmatizer()
    def __call__(self, doc):
        return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]

class PorterTokenizer:
    def __init__(self):
        self.stemmer = PorterStemmer()
    def __call__(self, doc):
        return [self.stemmer.stem(t) for t in word_tokenize(doc)]
