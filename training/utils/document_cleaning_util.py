"""
Utility functions to clean up documents before classification
"""

import re

def extract_article(article):
    """
    Journal article specific pre-processing

    - Removes references. The title of referenced articles may be useful
    but they introduce a lot of spurious features
    - Declarations: e.g. data availability, conflict of interest
    - Author list removal
    - Conversion to lower case
    - Remove numbers and special chars

    """
    # references and footnotes may be removed by the later regexs
    # but it's inconsistent so simpler to remove them
    article = re.sub(r'Footnotes\s+\n+[\s\S]*', '', article)
    article = re.sub(r'References\s+\n+[\s\S]*', '', article)

    # Remove author lists from the start
    # this isn't a perfect regex and could be improved
    article_front = article[:1000]
    article_front = re.sub(
        r'(\S+ ([A-Z].\s)?[\S\-]+ \d((,\d+)*)[\*†]?)',
        '',
        article_front
    )
    article = article_front + article[1000:]

    # boilerplate sections, different journals include
    # some of these on some papers so it's easier to
    # scrub all of them
    article = re.sub(
        r'Author Contributions\s+\n+[\s\S]*', '', article)
    article = re.sub(
        r'Data Availability Statement[\s\S]*', '', article)
    article = re.sub(r'Conflict of Interest[\s\S]*', '', article)
    article = re.sub(r'Funding[\s\S]*', '', article)
    article = re.sub(r'Received:  [\s\S]*', '', article)
    article = re.sub(r'Reviewed by: [\s\S]*', '', article)

    article = article.lower()
    article = re.sub('[^A-Za-z]', ' ', article)
    return article
