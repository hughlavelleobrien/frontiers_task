"""
Basic tests checking functionality of the server
"""

import json
from fastapi.testclient import TestClient

from main import app

client = TestClient(app)

def test_health_check():
    """
    Check the health check works to find loading issues
    """
    response = client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"health": "all good"}

def test_suggest():
    """
    Check the journal suggestion endpoint responds correctly
    to text payload
    """
    with open('test_data/predict_test_input.json') as f:
        test_data = json.load(f)
    response = client.post(
        "/suggest",
        json=test_data[0]
    )

    assert response.status_code == 200
    assert response.json() == {'journal': 'Frontiers in Medicine'}

def test_suggest_pdf():
    """
    Check the journal suggestion with pdf endpoint
    responds with a journal to a valid pdf file
    """
    response = client.post(
        "/suggest_pdf",
        files={"pdf": (
            "filename",
            open("test_data/test_pdf.pdf", "rb"),
            "pdf")
        }
    )

    assert response.status_code == 200
    assert list(response.json().keys()) == ['journal']
