"""
Frontiers article prediction service
"""
import os
from typing import Optional
from io import BytesIO

from fastapi import FastAPI, Response, File, UploadFile
from pydantic import BaseModel
from pdfminer.high_level import extract_text

from sklearn_classifier import SklearnClassifier

app = FastAPI()

# build classifier class based on env_vars
CLASSIFIER = None
if os.environ['MODE'] == 'sklearn':
    CLASSIFIER = SklearnClassifier(os.environ['MODEL_NAME'])
else:
    raise ValueError("Invalid MODE configured")


@app.get("/health")
def health(response: Response):
    """
    Endpoint to ping for up message
    """
    response.status_code = 200
    return {"health": "all good"}


class ArticleRequest(BaseModel):
    """
    Class to parse article request payloads
    """
    text: str = ''

@app.post("/suggest")
def suggest(article: Optional[ArticleRequest] = None):
    """
    Endpoint for classifying text input
    """
    journal = CLASSIFIER.classify(article.text)
    return {"journal": journal}

@app.post("/suggest_pdf")
def suggest_pdf(pdf: UploadFile = File(...)):
    """
    Endpoint for classing from pdf input
    """
    contents = pdf.file.read()
    extracted_text = extract_text(BytesIO(contents))
    journal = CLASSIFIER.classify(extracted_text)
    return {"journal": journal}
