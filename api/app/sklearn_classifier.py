"""
v1 version to classify input based on SciKit-Learn based classifiers
"""

from joblib import load
from utils.tokenizer import LemmaTokenizer, PorterTokenizer

from utils.document_cleaning_util import extract_article

class SklearnClassifier():
    """
    Class to run scikit-learn classifiers
    """
    def __init__(self, model_name):
        # load in the classifier
        print("LOADING MODEL...")
        self.clf = load("models/" + model_name + ".joblib")
        self.tfidf_transformer = load(
            "models/" + model_name + "-tfidf-transformer.joblib")
        self.vectorizer = load(
            'models/' + model_name + '-vectorizer.joblib')

        print("MODEL LOADED", self.clf)

    def classify(self, input_text):
        """
        Classifies a given single input sample
        """
        input_array = self._pre_process(input_text)
        return self.clf.predict(input_array)[0]

    def _pre_process(self, input_text):
        """
        Extact the relevant text and runs the pre-processing steps
        """
        article = extract_article(input_text)
        article = self.vectorizer.transform([article])
        return self.tfidf_transformer.transform(article)
