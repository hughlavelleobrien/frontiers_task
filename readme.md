# Frontiers Journal Prediction Task

See `Assignment_for_ML_roles.pdf` for task instructions


## Layout

Training and API split into subfolders:

### Training:

Model generation using sklearn.

Sklearn implementation done as MVP to enable full testing in the minimal timeframe.

Improved performance would likely be obtained with a [SciBERT](https://github.com/allenai/scibert) or similar model with mappings to Frontiers journals. Additionally, journals with small volumes could use data from similar journals to boost dataset.

### API:

Dockerised FastAPI app capable of serving the sklearn classifiers.

Models configured via `/api/environment` file

Run with `docker-compose up article_prediction` in the root folder. Will be served on port `5000` locally

Endpoints:

- GET `/health` - just returns a 200 if the app is up right now
- POST `/suggest` - takes a string and returns a predicted journal
- POST `/suggest_pdf` - does same prediction based on pdf, extacted with pdfminer

Run tests with `docker-compose exec article_prediction pytest -v`
